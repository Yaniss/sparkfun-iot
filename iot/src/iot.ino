/*
 * Project iot
 * Description:
 * Author:
 * Date:
 */

#include "SparkFunMicroOLED.h" // Include MicroOLED library
#include "math.h"

int led1 = D5;
int led2 = D4;
int motor = D1;
int buzzer = D3;
boolean on = false;
boolean ringing = false;

MicroOLED oled;
// setup() runs once, when the device is first turned on.
void setup()
{
        // Put initialization like pinMode and begin functions here.
        pinMode(led1, OUTPUT);
        pinMode(led2, OUTPUT);
        pinMode(motor, OUTPUT);
        pinMode(buzzer, OUTPUT);
        Particle.function("onLed", onLed);
        Particle.function("offLed", offLed);
        Particle.function("lock", lock);
        Particle.function("unlock", unlock);
        Particle.function("startRing", startRing);
        Particle.function("stopRing", stopRing);
        Particle.function("goStraight", upArrow);
        Particle.function("goLeft", leftArrow);
        Particle.function("goRight", rightArrow);
        Particle.function("goBack", backArrow);
        Particle.function("clearScreen", clearScreen);

        analogWrite(motor, 14, 50);
        delay(1000);

        oled.begin(); // Initialize the OLED
        oled.clear(ALL); // Clear the display's internal memory
        oled.display(); // Display what's in the buffer (splashscreen)
        delay(1000);
        oled.clear(ALL);
}

int onLed(String data)
{
        digitalWrite(led1, HIGH);
        digitalWrite(led2, HIGH);
        return (0);
}

int offLed(String data)
{
        digitalWrite(led1, LOW);
        digitalWrite(led2, LOW);
        return (0);
}

int lock(String data)
{
        analogWrite(motor, 14, 50);
        delay(1000);
        return (0);
}

int unlock(String data)
{
        analogWrite(motor, 25, 50);
        delay(1000);
        return (0);
}

int startRing(String data)
{
        ringing = true;
        while (ringing) {
                tone(buzzer, 440, 1000);
                delay(1000);
                tone(buzzer, 880, 1000);
                delay(1000);
        }

        return (0);
}

int stopRing(String data)
{
        ringing = false;
        delay(2000);
        noTone(buzzer);
        return (0);
}

int upArrow(String data)
{
        printTitle("Front", 1);
        return 1;
}

int leftArrow(String data)
{
        printTitle("Left", 1);
        return 1;
}

int rightArrow(String data)
{
        printTitle("Right", 1);
        return 1;
}

int backArrow(String data)
{
        printTitle("Back", 1);
        return 1;
}

int clearScreen(String data)
{
        oled.clear(ALL);
        return (0);
}

void printTitle(String title, int font)
{
        int middleX = oled.getLCDWidth() / 2;
        int middleY = oled.getLCDHeight() / 2;

        oled.clear(PAGE);
        oled.setFontType(font);
        // Try to set the cursor in the middle of the screen
        oled.setCursor(middleX - (oled.getFontWidth() * (title.length() / 2)),
            middleY - (oled.getFontWidth() / 2));
        // Print the title:
        oled.print(title);
        oled.display();
        delay(1500);
        oled.clear(PAGE);
}

// loop() runs over and over again, as quickly as it can execute.
void loop()
{
        // The core of your code will likely live here.
}
